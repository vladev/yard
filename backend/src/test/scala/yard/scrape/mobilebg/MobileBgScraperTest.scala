package yard.scrape.mobilebg

import org.scalatest.FunSuite
import yard.scrape.PageLoader
import org.jsoup.nodes.Document
import org.jsoup.Jsoup
import java.io.File
import org.scalatest.matchers.ShouldMatchers
import yard.ad.{ModelWithMake, Make}

class MobileBgScraperTest extends FunSuite with ShouldMatchers {
  val s = new MobileBgScraper(Map(), Map())

  test("parse short mobile.bg url") {
    val id = s.adId("http://www.mobile.bg/11367574519034029")

    id should be (Some("11367574519034029"))
  }

  test("parse long mobile.bg url") {
    val id = s.adId("http://nfs.mobile.bg/pcgi/mobile.cgi?topmenu=1&act=4&adv=11367574519034029&f1=details&slink=35pcc2")

    id should be (Some("11367574519034029"))
  }

  test("wrong domain") {
    // Note the .de
    val id = s.adId("http://www.mobile.de/11367574519034029")

    id should be (None)
  }

  test("parse page") {
    val subaru = Make(Some(88), "Subaru")
    val makes = Map(88 -> subaru)
    val legacy = ModelWithMake(Some(1234), "Legacy", subaru)
    val models = Map(1234 -> legacy)

    val s = new MobileBgScraper(makes, models) with PageLoader {
      override def load(url: String): Document = {
        Jsoup.parse(new File(this.getClass.getResource("/mobile.bg.html").toURI), "windows-1251")
      }
    }

    val result = s("http://www.mobile.bg/11372020967652913")
    result should be ('defined)
    val car = result.get.car

    car.modelId should equal (legacy.id.get)
    car.powerInKW should equal (260)
    car.mileage should equal (Some(145000))
  }

  test("parse missing page") {
    val s = new MobileBgScraper(Map(), Map()) with PageLoader {
      override def load(url: String): Document = Jsoup.parse("")
    }

    s("http://www.mobile.bg/11372020967652913") should be (None)
  }
}
