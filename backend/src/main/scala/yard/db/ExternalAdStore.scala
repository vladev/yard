package yard.db

import yard.ad.{SimpleCar, ExternalAd}

trait ExternalAdStore {
  this: SimpleCarStore with Profile =>

  import profile.simple._

  object externalAdTable extends Table[ExternalAd]("external_ad") {
    def id = column[Int]("external_ad_id", O.PrimaryKey, O.AutoInc)

    def externalId = column[String]("external_id", O.NotNull)

    def carId = column[Int]("car_id", O.NotNull)

    def * = id.? ~ externalId ~ carId <>(ExternalAd, ExternalAd.unapply _)

    def car = foreignKey("FK_external_ad_car", carId, simpleCarTable)(_.id)
  }

  object externalAd {
    def insert(host: String, externalId: String, carId: Int)(implicit session: Session) = {
      externalAdTable.externalId ~ externalAdTable.carId insert(extId(host, externalId), carId)
    }

    def loadById(host: String, externalId: String)(implicit session: Session): Option[SimpleCar] = {
      val eId = extId(host, externalId)
      val q = for {
        s <- simpleCarTable
        e <- externalAdTable if e.externalId === eId
      } yield s

      q.firstOption()
    }

    private def extId(host: String, externalId: String) = s"$host:$externalId"
  }

}
