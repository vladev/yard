package yard.db

import yard.ad.Make

trait MakeStore {
  this: Profile =>

  import profile.simple._
  import scala.slick.lifted.Query

  object makeTable extends Table[Make]("makes") {
    def id = column[Int]("make_id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name", O.NotNull)

    def * = id.? ~ name <>(Make.apply _, Make.unapply _)
  }

  object make {

    def all(implicit session: Session) = Query(makeTable).list

    private[db] def insert(m: Make)(implicit session: Session) = {
      makeTable.name returning makeTable.id insert m.name
    }
  }

}
