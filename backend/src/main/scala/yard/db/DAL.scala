package yard.db

import scala.slick.driver.ExtendedProfile

class DAL(override val profile: ExtendedProfile) extends
MakeStore with ModelStore with SimpleCarStore with ExternalAdStore with Profile {

  import profile.simple._

  val ddl = makeTable.ddl ++ modelTable.ddl ++ simpleCarTable.ddl ++ externalAdTable.ddl

  def create(implicit session: Session) {
    ddl.create
  }

  def drop(implicit session: Session) {
    ddl.drop
  }
}
