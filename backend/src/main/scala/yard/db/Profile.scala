package yard.db

import scala.slick.driver.ExtendedProfile

trait Profile {
  val profile: ExtendedProfile
}