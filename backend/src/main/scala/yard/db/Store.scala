package yard.db


import scala.slick.session.{Database, Session}
import scala.slick.lifted.{DDL, SimpleFunction}

class Store(val db: Database, val dal: DAL)

object Store {
  def jdbcToDriver(driver: String) = driver match {
    case "org.h2.Driver" => scala.slick.driver.H2Driver
    case "org.postgresql.Driver" => scala.slick.driver.PostgresDriver
  }

  def apply(driver: String, database: String, user: String) = new Store(
    Database.forURL(database, user = user, driver = driver),
    new DAL(jdbcToDriver(driver)))
}
