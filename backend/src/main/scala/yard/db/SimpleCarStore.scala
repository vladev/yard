package yard.db

import java.sql.Date
import yard.ad.simplecar._
import yard.ad.{Fuel, Body, Gearbox, SimpleCar}


trait SimpleCarStore {
  this: ModelStore with Profile =>

  import profile.simple._
  import scala.slick.lifted.{Query, MappedTypeMapper}

  object simpleCarTable extends Table[SimpleCar]("cars") {
    implicit val bodyStyleMapper =
      MappedTypeMapper.base[Body.Style, String](
      _.toString, {
        case "Sedan" => Body.Sedan
        case "Estate" => Body.Estate
        case "Coupe" => Body.Coupe
        case "Convertible" => Body.Convertible
      }
      )

    implicit val gearboxTypeMapper =
      MappedTypeMapper.base[Gearbox.Type, String](
      _.toString, {
        case "Manual" => Gearbox.Manual
        case "Automatic" => Gearbox.Automatic
        case "CVT" => Gearbox.CVT
        case "Semiautomatic" => Gearbox.Semiautomatic
      }
      )

    implicit val fuelTypeMapper =
      MappedTypeMapper.base[Fuel.Type, String](
      _.toString, {
        case "Diesel" => Fuel.Diesel
        case "Petrol" => Fuel.Petrol
        case "Hybrid" => Fuel.Hybrid
      }
      )

    def id = column[Int]("car_id", O.PrimaryKey, O.AutoInc)

    def modelId = column[Int]("model_id", O.NotNull)

    def powerInKW = column[Int]("power_in_kw", O.NotNull)

    def fuelType = column[Fuel.Type]("fuel_type", O.NotNull)

    def productionDate = column[Date]("production_date", O.NotNull)

    def gearboxType = column[Gearbox.Type]("gearbox_type", O.NotNull)

    def mileage = column[Int]("mileage")

    def bodyStyle = column[Body.Style]("body_style", O.NotNull)

    def model = foreignKey("FK_car_model", modelId, modelTable)(_.id)

    def * = id.? ~
      modelId ~
      powerInKW ~
      fuelType ~
      productionDate ~
      gearboxType ~
      mileage.? ~
      bodyStyle <>(SimpleCar.apply _, SimpleCar.unapply _)

  }

  object simpleCar {

    def insert(m: SimpleCar)(implicit session: Session) = {
      simpleCarTable.modelId ~
        simpleCarTable.powerInKW ~
        simpleCarTable.fuelType ~
        simpleCarTable.productionDate ~
        simpleCarTable.gearboxType ~
        simpleCarTable.mileage.? ~
        simpleCarTable.bodyStyle returning simpleCarTable.id insert(m.modelId, m.powerInKW,
        m.fuelType, m.productionDate, m.gearboxType, m.mileage, m.bodyStyle)
    }
  }

}



