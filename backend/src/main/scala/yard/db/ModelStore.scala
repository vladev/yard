package yard.db

import yard.ad.{ModelWithMake, Model}

trait ModelStore {
  this: MakeStore with Profile =>

  import profile.simple._

  object modelTable extends Table[Model]("models") {
    def id = column[Int]("model_id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name", O.NotNull)

    def makeId = column[Int]("make_id", O.NotNull)

    def * = id.? ~ name ~ makeId <>(Model.apply _, Model.unapply _)

    def make = foreignKey("FK_model_make", makeId, makeTable)(_.id)

  }

  object model {
    def all(implicit session: Session) = (for (m <- modelTable) yield m).list

    private[db] def insert(m: Model)(implicit session: Session) = {
      modelTable.name ~ modelTable.makeId returning modelTable.id insert(m.name, m.makeId)
    }

    def withMakes(implicit session: Session): Map[Int, ModelWithMake] =
      (for {
        (model, make) <- modelTable innerJoin makeTable on (_.makeId === _.id)
      } yield (model, make)).list.map {
        case (model, make) => (model.id.get, ModelWithMake(model.id, model.name, make))
      }.toMap
  }

}
