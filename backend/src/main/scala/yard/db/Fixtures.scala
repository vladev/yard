package yard.db

import slick.session.{Session, Database}
import yard.ad.{Model, Make}

object Fixtures {
  private val makesAndModels = List(
    ("Alfa Romeo", List("156", "159", "Brera", "145", "147", "75", "Mito")),
    ("BMW", List("1-series", "3-series", "5-series", "6-series", "7-series", "8-series")),
    ("Subaru", List("Impreza", "Legacy", "Forester"))
  )

  def insertMakesAndModels(dal: DAL)(implicit session: Session) {
    session withTransaction {
      for ((make, models) <- makesAndModels) {
        val id = dal.make insert Make(None, make)

        for (modelName <- models) {
          dal.model insert Model(None, modelName, id)
        }
      }
    }
  }
}
