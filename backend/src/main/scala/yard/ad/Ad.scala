package yard.ad

import scala.slick.session.{Session, Database}
import yard.scrape.Scraper
import grizzled.slf4j.Logging
import scala.util.{Failure, Success, Try}
import yard.db.DAL

case class ExternalAd(id: Option[Int],
                      externalId: String,
                      carId: Int)

class Ad private(val db: Database, val scraper: Scraper, val dal: DAL) extends Logging {

  def load(url: String): Option[SimpleCar] = {
    val adId = scraper.adId(url)
    // TODO: Try to load from db
    logger.info(s"Scraping: $url")

    Try(scraper(url)) match {
      case Success(Some(ad)) =>
        db withSession {
          implicit session: Session =>
            dal.simpleCar insert ad.car
        }

        Some(ad.car)

      case Success(None) =>
        logger.info("Bad page")
        None

      case Failure(e) =>
        logger.error("Invalid url", e)
        None

    }
  }
}

//object Ad {
//  def apply(db: Database): Ad = {
//    db withSession {
//      implicit session: Session =>
//        val modelsWithMake = ModelStore.withMakes
//
//        val makes = modelsWithMake map {
//          case (_id, m) => (m.make.id.get, m.make)
//        }
//        val scraper = new MobileBgScraper(makes, modelsWithMake)
//        new Ad(db, scraper)
//    }
//  }
//}
