package yard.ad

import java.util.Date

case class ExtendedCar(id: Option[Int],
               model: Model,
               productionDate: Date,
               engine: Engine,
               gearbox: Gearbox,
               mileage: Int,
               bodyStyle: Body.Style,
               consumption: Consumption)


object Engine {
  sealed trait Shape
  case object Inline extends Shape
  case object V extends Shape
  case object Boxer extends Shape

  //  case object Hybrid extends Fuel

  implicit class KW(val amount: Int) extends AnyVal
}
case class Engine(id: Option[Int], name: String, displacement: Int,
                  cylinders: Int, shape: Engine.Shape,
                  fuel: Fuel.Type, power: Engine.KW,
                  euroEmissions: Int)



object Consumption {
  implicit class L100(val liters: Double) extends AnyVal
}
case class Consumption(urban: Consumption.L100,
                       highway: Consumption.L100,
                       mixed: Consumption.L100)



object Templates {
  val alfa = Make(None, "Alfa Romeo")
  val alfa145 = alfa.model("145")
  val alfa156 = alfa.model("156")
  val alfa159 = alfa.model("159")

  val jtd140 = Engine(None, name = "2.4 JTD", displacement = 2378,
    cylinders = 5, shape = Engine.Inline, fuel = Fuel.Diesel, power = 103,
    euroEmissions = 3)

  val jtd105 = Engine(None, name = "1.9 JTD", displacement = 1910,
    cylinders = 4, shape = Engine.Inline, fuel = Fuel.Diesel, power = 77,
    euroEmissions = 2)

  val ts20 = Engine(None, name = "2.0 TS", displacement = 1997,
    cylinders = 4, shape = Engine.Inline, fuel = Fuel.Petrol, power = 121,
    euroEmissions = 2)

  val ts20e3 = Engine(None, name = "2.0 TS", displacement = 1997,
    cylinders = 4, shape = Engine.Inline, fuel = Fuel.Petrol, power = 119,
    euroEmissions = 3)

  val enginesPerModel: Map[Model, List[(Engine, Consumption)]] = Map(
    alfa145 -> ((ts20, Consumption(12, 8, 10)) :: (jtd105, Consumption(9, 6, 7.5)) :: Nil)
    //    alfa156 -> List(ts20, ts20e3, jtd105, jtd140)
  )

  //  val availableGearboxes: Map[Engine, List[Gearbox]] = Map(
  //    jtd140 -> List(Gearbox(None, "", 5, Gearbox.Manual)),
  //    ts20 -> List(Gearbox.Manual, Gearbox.Semiautomatic)
  //  )
  //
  //  // TODO: Move to JodaTime
  //  val ad = Car(None, alfa156, new Date(), jtd140,
  //    Gearbox.Manual, 144000, Style.Estate, Consumption(9, 6, 7.5))
  //  )


}
