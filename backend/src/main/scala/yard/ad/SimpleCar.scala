package yard.ad

import java.sql.Date

case class Make(id: Option[Int], name: String) {
  def model(name: String, modelId: Option[Int] = None): Model =
    Model(modelId, name, this.id.get)
}

case class Model(id: Option[Int], name: String, makeId: Int)
case class ModelWithMake(id: Option[Int], name: String, make: Make)

object Gearbox {
  sealed trait Type
  case object Manual extends Type
  case object Automatic extends Type
  case object CVT extends Type
  case object Semiautomatic extends Type
}
// Used by ExtendedCar
case class Gearbox(id: Option[Int], name: String, gears: Int, `type`: Gearbox.Type)

object Body {
  sealed trait Style
  case object Sedan extends Style
  case object Estate extends Style
  case object Coupe extends Style
  case object Convertible extends Style
  case object Van extends Style
  case object Pickup extends Style
  case object Hatchback extends Style
}

object Fuel {
  sealed trait Type
  case object Petrol extends Type
  case object Diesel extends Type
  case object Hybrid extends Type
}

case class SimpleCar(id: Option[Int],
                     modelId: Int,
                     powerInKW: Int,
                     fuelType: Fuel.Type,
                     productionDate: Date,
                     gearboxType: Gearbox.Type,
                     mileage: Option[Int],
                     bodyStyle: Body.Style)
