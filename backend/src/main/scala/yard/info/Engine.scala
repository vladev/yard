package yard.info


sealed trait FuelType

case object Petrol extends FuelType

case object Diesel extends FuelType


case class Engine(id: Option[Int],
                  power: Int,
                  fuel: FuelType)
