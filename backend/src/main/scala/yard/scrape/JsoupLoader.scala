package yard.scrape

import org.jsoup.nodes.Document
import org.jsoup.Jsoup
import grizzled.slf4j.Logging

trait JsoupLoader extends PageLoader with Logging {
  def load(url: String): Document = {
    logger.debug(s"Sync fetch of $url")
    Jsoup.connect(url).get()
  }

}
