package yard.scrape.mobilebg

import java.sql.Date
import org.jsoup.select.Elements
import scala.collection.JavaConversions._
import grizzled.slf4j.Logging
import yard.scrape.{JsoupLoader, ScrapedAd, Scraper}
import com.github.theon.uri.Uri._
import yard.ad._
import yard.ad.SimpleCar
import yard.scrape.ScrapedAd
import scala.Some
import yard.ad.ModelWithMake
import yard.ad.Make


class MobileBgScraper(val makes: Map[Int, Make], val models: Map[Int, ModelWithMake])
  extends Scraper with Logging with JsoupLoader {

  logger.debug(s"Models used: $models")

  val engineMap: Map[String, Fuel.Type] = Map(
    "Бензинов" -> Fuel.Petrol,
    "Дизелов" -> Fuel.Diesel,
    "Хибриден" -> Fuel.Hybrid
  )

  val gearboxMap: Map[String, Gearbox.Type] = Map(
    "Ръчна" -> Gearbox.Manual,
    "Автоматична" -> Gearbox.Automatic,
    "Полуавтоматична" -> Gearbox.Semiautomatic
  )

  val months = List(
    "януари",
    "февруари",
    "март",
    "април",
    "май",
    "юни",
    "юли",
    "август",
    "септември",
    "октомври",
    "ноември",
    "декември"
  )

  val bodyStyleMap: Map[String, Body.Style] = Map(
    "Ван" -> Body.Van,
    "Кабрио" -> Body.Convertible,
    "Катафалка" -> Body.Estate,
    "Комби" -> Body.Estate,
    "Купе" -> Body.Coupe,
    "Линейка" -> Body.Van,
    "Миниван" -> Body.Van,
    "Пикап" -> Body.Pickup,
    "Седан" -> Body.Sedan,
    "Стреч лимузина" -> Body.Sedan,
    "Хечбек" -> Body.Hatchback
  )

  override def apply(url: String): Option[ScrapedAd] = {
    val mobileBgId = adId(url)

    if (mobileBgId.isEmpty) return None

    val page = this.load(url)

    val container = page.select("html body center table tbody tr td form table tbody tr td")

    val infoBox = container.select("span")
//    logger.debug(s"Info box: $infoBox")
    val imageBox = container.select("table")
//    logger.debug(s"Image box: $imageBox")

    if (infoBox.isEmpty || imageBox.isEmpty) return None

    val title = infoBox.first().ownText()
    val powerText = section(infoBox, "Мощност")
    val fuelText = section(infoBox, "Тип двигател")
    val gearboxText = section(infoBox, "Скоростна кутия")
    val dateText = section(infoBox, "Дата на производство")
    val mileageText = section(infoBox, "Пробег")
    val bodyStyleText = section(infoBox, "Категория")
    val imageUrls = collectImageUrls(imageBox)



    val model = extractModelWithMake(title)
    logger.debug(s"Model with make: $title -> $model")

    val powerKW = extractPowerInKW(powerText)
    logger.debug(s"Power: $powerText -> $powerKW")

    val fuelType = engineMap.get(fuelText)
    logger.debug(s"Fuel: $fuelText -> $fuelType")

    val produced = extractProductionDate(dateText)
    logger.debug(s"Produced: $dateText -> $produced")

    val mileageKM = extractMileage(mileageText)
    logger.debug(s"Mileage: $mileageText -> $mileageKM")

    val gearboxType = gearboxMap.get(gearboxText)
    logger.debug(s"Gearbox: $gearboxText -> $gearboxType")

    val bodyStyle = bodyStyleMap.get(bodyStyleText)
    logger.debug(s"Body: $bodyStyleText -> $bodyStyle")

    val car = for {
      m <- model
      power <- powerKW
      fuel <- fuelType
      date <- produced
      gearbox <- gearboxType
      body <- bodyStyle
    } yield SimpleCar(id = None, modelId = m.id.get, powerInKW = power,
        fuelType = fuel, productionDate = date,
        gearboxType = gearbox, mileage = mileageKM, bodyStyle = body)

    if (car.isEmpty) return None

    Some(ScrapedAd(id = mobileBgId.get, car = car.get, photos = imageUrls))
  }

  private def section(infoBox: Elements, caption: String): String = {
    logger.debug(s"Loading $caption")
    val box = infoBox.select(s"table tbody tr:contains($caption").select("td")
    if (box.isEmpty) return ""

    box.last().text()
  }

  private def extractMake(title: String): Option[(Make, String)] = {
    val foundMake = makes collectFirst {
      case (id, make) if title.toLowerCase.trim startsWith make.name.toLowerCase => (id, make)
    }

    foundMake match {
      case None => None
      case Some((id, make)) =>
        Some((make, title substring make.name.length))
    }
  }

  private def extractModel(make: Make, title: String): Option[ModelWithMake] =
    models collectFirst {
      case (id, model) if (model.make == make
        && title.toLowerCase.trim.startsWith(model.name.toLowerCase)) => model
    }

  private def extractModelWithMake(title: String): Option[ModelWithMake] =
    for {
      makeAndTitle <- extractMake(title)
      model <- extractModel(makeAndTitle._1, makeAndTitle._2)
    } yield model

  private def extractPowerInKW(powerText: String): Option[Int] = {
    if (powerText.isEmpty) return None
    val powerInHP = powerText.split(' ')(0).toInt
    Some((powerInHP * 0.745699872).toInt)
  }

  private def extractProductionDate(dateText: String): Option[Date] = {
    val parts = dateText.split(' ')
    if (parts.length < 2) return None

    val month = months.indexOf(parts(0)) + 1
    if (month == 0) return None

    val year = parts(1).toInt

    Some(Date.valueOf(s"$year-$month-01"))
  }

  private def extractMileage(mileageText: String): Option[Int] = {
    if (mileageText.length > 0) {
      Some(mileageText.split(' ')(0).toInt)
    } else {
      None
    }

  }


  private def collectImageUrls(imagesArea: Elements): List[String] = {
    val smallUrls = imagesArea.select("a[href^=javascript:showallpicts] img")
      .toList.map(_.attr("src"))

    // first: http://www.mobile.bg/photos/1/11364902835285432_1.pic
    // big:   http://www.mobile.bg/photos/1/big/11364902835285432_1.pic
    // rest:  http://www.mobile.bg/photos/1/small/11364902835285432_2.pic
    smallUrls match {
      case first :: rest =>
        val other = for {
          img <- rest
        } yield img.replaceFirst( """photos/(\d+)/small""", "photos/$1/big")

        first.replaceFirst( """photos/(\d+)""", "photos/$1/big") :: other
      case Nil => Nil
    }
  }

  def adId(rawUri: String): Option[String] = {
    val uri = parseUri(rawUri)
    uri.host match {
      case None => None
      case Some(host) =>
        if (host.endsWith("mobile.bg")) {
          val path = uri.path.stripPrefix("/")
          if (path.forall(_.isDigit)) {
            Some(path)
          } else {
            uri.query.params.get("adv").map(_.head)
          }
        } else {
          None
        }
    }
  }
}