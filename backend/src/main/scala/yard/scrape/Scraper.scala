package yard.scrape

import yard.ad._
import SimpleCar._

case class ScrapedAd(id: String,
                     car: SimpleCar,
                     photos: List[String])

trait Scraper extends (String => Option[ScrapedAd]) with PageLoader {
  def adId(url: String): Option[String]
}

