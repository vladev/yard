package yard.scrape

import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import java.io.{FileOutputStream, File}
import org.jsoup.Jsoup
import grizzled.slf4j.Logging

class ImageDownloader extends ((List[String], File) => Unit) with Logging {
  def apply(urls: List[String], target: File) {
    val results = for {
      (u, i) <- urls.zipWithIndex
    } yield future {
      download(u, new File(target, s"${i + 1}.jpg"))
    }

    Await.result(Future.sequence(results), 10.seconds)
  }

  private def download(url: String, target: File) {
    logger.debug(s"Downloading $url at ${target.getAbsolutePath}")

    val imgData = Jsoup.connect(url).ignoreContentType(true).execute()
    val ct = imgData.header("Content-Type")

    if (ct != "application/octet-stream") {
      logger.warn(s"Not Downloading $url - Content-Type: $ct")
    } else {
      val out = new FileOutputStream(target)
      out.write(imgData.bodyAsBytes())
      out.close()
    }
  }

}

