package yard.scrape

import org.jsoup.nodes.Document

trait PageLoader {
  def load(url: String): Document
}
