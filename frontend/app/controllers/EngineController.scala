package controllers

import yard.info.{Engine, Petrol}
import play.api.mvc.{Action, Controller}

object EngineController extends Controller {
  def index = Action {
    Ok(views.html.engine.index(Engine(None, 100, Petrol)))
  }
}
