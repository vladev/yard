package controllers

import yard.info.{FuelType, Diesel, Engine, Petrol}
import play.api.mvc.{Action, Controller}
import play.api.data._
import play.api.data.format.Formatter
import play.api.data.Forms._

object AdminController extends Controller {
  implicit def fuelTypeFormatter = new Formatter[FuelType] {
    def bind(key: String, data: Map[String, String]): Either[Seq[FormError], FuelType] =
      data.get(key).collect {
        case "diesel" => Diesel
        case "petrol" => Petrol
      }.toRight(Seq(FormError(key, "engine.format", Nil)))

    def unbind(key: String, value: FuelType): Map[String, String] =
      Map(key -> value.toString.toLowerCase)
  }

  val fuelType = of[FuelType]

  val engineForm = Form(
    mapping(
      "id" -> optional(number),
      "power" -> number,
      "fuel" -> fuelType
    )(Engine.apply)(Engine.unapply)
  )

  def engine = Action {
//    val m = Map(
//      "name" -> "Alfa",
//      "id" -> "1",
//      "fuel" -> "diesel1"
//    )
//
//    println(engineForm.bind(m).errors)

    Ok(views.html.admin.engine.edit.render(engineForm))
  }
}
