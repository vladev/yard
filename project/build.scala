import sbt._
import Keys._

object YardBuild extends Build {
  val yardSettings = Seq(
    scalaVersion := "2.10.2"
  )

  val backendSettings = yardSettings ++ Seq(
    libraryDependencies := dependencies.backendDeps
  )

  val backend =
    Project("backend", file("backend"))
      .settings(backendSettings:_*)


  val frontend =
    play.Project("frontend", path = file("frontend"))
      .settings(yardSettings: _*)
      .dependsOn(backend)


  val yard =
    Project("yard", file("."))
      .settings(yardSettings: _*)
      .dependsOn(frontend, backend)

}