import sbt._

object dependencies {
  val slick = "com.typesafe.slick" %% "slick" % "1.0.1"
  val postgresql = "postgresql" % "postgresql" % "9.1-901.jdbc4"
  val jsoup = "org.jsoup" % "jsoup" % "1.7.2"
  val scalaUri = "com.github.theon" %% "scala-uri" % "0.3.5"
  val logback = "ch.qos.logback" % "logback-classic" % "1.0.11"
  val slf4j = "org.slf4j" % "slf4j-api" % "1.7.5"
  val grizzledSlf4j = "org.clapper" % "grizzled-slf4j_2.10" % "1.0.1"

  val scalatest = "org.scalatest" %% "scalatest" % "1.9.1" % "test"

  val backendDeps = Seq(
    slick, postgresql, jsoup, scalaUri, logback, slf4j, grizzledSlf4j,
    scalatest
  )
}